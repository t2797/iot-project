#  IoT Project

### Basic project to perform the basic configuration of IoT devices

The project consists of a small REST API to manage the configuration of IoT devices. It has an H2-based in-memory database and an in-memory authentication system to protect JWT based management operations.

The application uses Swagger to document the API.

# Assumptions

Based on the requirements and the time available, it is assumed that:

- It is not necessary to manage devices or SIMs.
- REST API manage the configuration process of the devices.
- The device status and the SIM status are different and independent status.
- The devices and SIM are currently created in the database.
- The device only could be sell if it has a READY status, a temperature between the determined threshold and an associated SIM. SIM status does not apply.
- The creation of a SIM could be done in the configuration process, for reasons of time, it is decided to simplify the process and associate an existing device and a SIM.
- All data is stored into an in-memory database to simplify testing.

# Main libraries used

- Spring Boot 2.5.4
- H2 Database
- SpringFox 3.0.0
- Spring Data 2.5.4
- Spring Security 2.5.4
- Java 1.8

# Features

The following table lists the REST API features:

|Feature| Description  | Path|
|--|--|--|
| Authentication | Returns a JWT token to access protected resources | /login|
| Devices | Returns devices by status and the status of their related SIMs| /devices/**|
| SIM | Returns SIMs not associated a device | /sim/** |
| Configuration process | Update or remove a configuration device status | /configuration/** |
|Database| Access to embedded H2 database| /h2-console/login.jsp|

## Embedded database

The embedded database is loaded using a file in the project called *data.sql* placed in resources.
It is possible to access the online console to explore the loaded data via the path */ h2-console / login.jsp.*


![img.png](src/main/resources/images/img.png)


## Swagger UI

REST API specification is described by Swagger UI. This web interface allow know all the endpoints and perform requests to the REST API.

The Swagger UI has an authorization mode configured to include the JWT token in requests. To include the token it is necessary to press the *Authorize* button and write  **Bearer** and paste the token in the form, there must be a space between both words.

![img_1.png](src/main/resources/images/img_1.png)

## Tasks

Guide to use the REST API to fulfill the required tasks

### Returns all devices in the warehouse that are waiting for activation
There are 3 SIMs with state *Waiting for activation* and two sims associated to devices. Example:

    curl -X GET "http://localhost:8080/iot/api/devices/status/sim?simStatus=Waiting%20for%20activation" -H  "accept: application/json"

Returns 2 devices.

### Management endpoint that enable the shop to remove or update a device configuration status.

Two protected endpoints, it is necessary get a token previously and set the token in the header.
By default exists two preconfigured devices with *READY* status and two devices with *NOT READY* status.

- Update device configuration needs the device ID and the SIM identifier. Example:

  curl -X POST "http://localhost:8080/iot/api/configuration/update" -H  "accept: application/json" -H  "Authorization: Bearer token..." -H  "Content-Type: application/json" -d "{  \"deviceId\": 2,  \"icc\": \"8982343199761726354\"}"

- Remove configuration only needs the device ID

  curl -X POST "http://localhost:8080/iot/api/configuration/remove/2" -H  "accept: application/json" -H  "Authorization: Bearer token..." -d ""

### Returns an ordered result of devices available for sale

By default, exists two devices with *READY* status. Example:

    curl -X GET "http://localhost:8080/iot/api/devices/status?sort=id%2Casc&status=READY" -H  "accept: application/json"


## Build

The project uses Apache Maven to build the application. To generate a JAR file, it is necessary to execute in the root of the project:

    mvn package

or:

    mvn install

Install process generate a Code Coverage Report using JaCoCo. The report is placed in: */target/site/jacoco*.

## Deploy

By default, the application uses port 8080 and the root path */iot/path/*. It is possible to deploy running:

    mvn spring-boot:run

By default, it runs in the following path:

    http://localhost:8080/iot/api/

User and database credentials are stored in the properties.

## Deploy with Docker

Application contains a Dockerfile file that generate a Docker image.  

After the JAR file has been generated (build completed), and placed in the root of the project, execute:

     docker build -t iot-project:1.0.0 .

then execute:

     docker run -p 8080:8080 iot-project:1.0.0

## Author

Cristian Mena - https://gitlab.com/t2797/iot-project