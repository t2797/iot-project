package com.process.iotproject.model.utils;

import io.swagger.annotations.ApiParam;
import lombok.Getter;
import lombok.Setter;

/**
 * Swagger has a know issue with Pageable interface of Spring Data.
 * It is necessary to define a new class to mage pageable properties in Swagger.
 *
 * https://github.com/springfox/springfox/issues/755
 */
@Getter
@Setter
public class SwaggerPageable {

    @ApiParam(value = "Number of elements per page")
    private Integer size = 20;

    @ApiParam(value = "Page you want to retrieve")
    private Integer page = 0;

    @ApiParam(value = "Sorting criteria. Format: property(,asc|desc)")
    private String sort;
}
