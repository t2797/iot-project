package com.process.iotproject.model.repositories;

import com.process.iotproject.model.entities.DeviceStatus;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DeviceStatusRepository extends JpaRepository<DeviceStatus, Long> {
}
