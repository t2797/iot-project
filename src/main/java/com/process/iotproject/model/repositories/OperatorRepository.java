package com.process.iotproject.model.repositories;

import com.process.iotproject.model.entities.Operator;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OperatorRepository extends JpaRepository<Operator, Long> {
}
