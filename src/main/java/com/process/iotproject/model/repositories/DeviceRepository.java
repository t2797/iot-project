package com.process.iotproject.model.repositories;

import com.process.iotproject.model.entities.Device;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;

public interface DeviceRepository extends PagingAndSortingRepository<Device, Long> {

    @Override
    List<Device> findAll();

    Page<Device> findAll(Pageable pageable);

    Page<Device> findAllByDeviceStatus_Id(Long statusId, Pageable pageable);

    Page<Device> findAllByDeviceStatus_Status(String status, Pageable pageable);

    Page<Device> findAllByDeviceStatus_StatusAndSimIsNotNull(String status, Pageable pageable);

    Page<Device> findAllByTemperature(int temperature, Pageable pageable);

    Page<Device> findAllByTemperatureAndDeviceStatus_Id(int temperature, Long statusId, Pageable pageable);

    Page<Device> findAllBySimIsNull(Pageable pageable);

    Page<Device> findAllBySimIsNotNull(Pageable pageable);

    Page<Device> findAllBySim_SimStatus_Id(Long id, Pageable pageable);

    Page<Device> findAllBySim_SimStatus_Status(String status, Pageable pageable);

    Optional<Device> findByName(String name);
}
