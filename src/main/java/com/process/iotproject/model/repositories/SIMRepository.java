package com.process.iotproject.model.repositories;

import com.process.iotproject.model.entities.SIM;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import java.util.List;
import java.util.Optional;

public interface SIMRepository extends PagingAndSortingRepository<SIM, Long>, QueryByExampleExecutor<SIM> {

    @Override
    List<SIM> findAll();

    Page<SIM> findByDeviceIsNull(Pageable pageable);

    List<SIM> findByDeviceIsNotNull();

    Page<SIM> findAllBySimStatus_Id(Long id, Pageable pageable);

    Page<SIM> findAll(Pageable pageable);

    Optional<SIM> findByIcc(String ICC);
}
