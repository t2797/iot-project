package com.process.iotproject.model.repositories;

import com.process.iotproject.model.entities.SIMStatus;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SIMStatusRepository extends JpaRepository<SIMStatus, Long> {
}
