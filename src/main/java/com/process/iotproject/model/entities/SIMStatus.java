package com.process.iotproject.model.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;

@Entity(name = "SIM_STATUS")
@Getter
@Setter
@ToString
@AllArgsConstructor
public class SIMStatus {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "status", nullable = false, unique = true, length = 30)
    private String status;

    public SIMStatus() {
        super();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        SIMStatus simStatus = (SIMStatus) o;
        return Objects.equals(id, simStatus.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }
}
