package com.process.iotproject.model.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
public class SIM {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @NotNull
    private Long id;

    @Column(name = "icc", nullable = false, unique = true, length = 19)
    @NotNull
    private String icc;

    @ManyToOne(optional = false)
    @JoinColumn(name = "sim_status_id", nullable = false)
    @NotNull
    private SIMStatus simStatus;

    @ManyToOne(optional = false)
    @JoinColumn(name = "country_id", nullable = false)
    @NotNull
    private Country country;

    @ManyToOne(optional = false)
    @JoinColumn(name = "operator_id", nullable = false)
    @NotNull
    private Operator operator;

    @OneToOne(mappedBy = "sim")
    @JsonBackReference
    private Device device;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        SIM sim = (SIM) o;
        return Objects.equals(id, sim.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }
}
