package com.process.iotproject.model.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;

@Entity(name = "OPERATOR")
@Getter
@Setter
@ToString
@AllArgsConstructor
public class Operator {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "code", length = 20, unique = true, nullable = false)
    private String code;

    public Operator() {
        super();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Operator operator = (Operator) o;
        return Objects.equals(id, operator.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }
}
