package com.process.iotproject.model.entities;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Device {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "name", nullable = false, unique = true)
    @NotNull
    private String name;

    @Column(name = "temperature")
    private int temperature;

    @ManyToOne(optional = false)
    @JoinColumn(name = "device_status_id", nullable = false)
    @NotNull
    private DeviceStatus deviceStatus;

    @OneToOne
    @JoinTable(name = "device_sim",
            joinColumns =
                    { @JoinColumn(name = "device_id", referencedColumnName = "id") },
            inverseJoinColumns =
                    { @JoinColumn(name = "sim_id", referencedColumnName = "id") })
    @JsonManagedReference
    private SIM sim;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Device device = (Device) o;
        return Objects.equals(id, device.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }
}
