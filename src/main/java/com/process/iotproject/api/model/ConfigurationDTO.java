package com.process.iotproject.api.model;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ConfigurationDTO {

    @NotNull
    private Long deviceId;

    @NotNull
    private String icc;
}
