package com.process.iotproject.api.model;

import lombok.Data;

@Data
public class JWTToken {

    private String token;

    public JWTToken(String token) {
        this.token = token;
    }
}
