package com.process.iotproject.api.controller;

import com.process.iotproject.model.entities.Device;
import com.process.iotproject.model.utils.Loggable;
import com.process.iotproject.services.device.DeviceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/devices", produces = "application/json")
@Api(value = "Devices controller", tags = "Device controller")
public class DeviceController {

    @Autowired
    DeviceService deviceService;


    @GetMapping(value = "/status/sim")
    @ApiOperation(value = "Get devices by SIM status", notes = "Return devices with a related SIM and the SIM status name")
    @Loggable
    public ResponseEntity<Page<Device>> getDevicesBySIMStatus(@RequestParam @ApiParam(allowableValues = "Active, Waiting for activation, Blocked, Deactivated", example = "Waiting for activation") String simStatus, Pageable pageable) {
        return new ResponseEntity<>(deviceService.getDevicesBySIMStatus(simStatus, pageable), HttpStatus.OK);
    }

    @GetMapping(value = "status")
    @ApiOperation(value = "Get devices by status", notes = "Return devices by the device status")
    @Loggable
    public ResponseEntity<Page<Device>> getDevicesByStatus(@RequestParam @ApiParam(allowableValues = "READY, NOT READY", example = "READY") String status, Pageable pageable) {
        return new ResponseEntity<>(deviceService.getDevicesByStatus(status, pageable), HttpStatus.OK);
    }

}
