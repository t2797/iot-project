package com.process.iotproject.api.controller;

import com.process.iotproject.api.model.ConfigurationDTO;
import com.process.iotproject.model.entities.Device;
import com.process.iotproject.model.utils.Loggable;
import com.process.iotproject.services.process.ConfigurationProcess;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/configuration", produces = "application/json")
@Api(value = "Configuration process controller", tags = "Configuration process controller")
public class ProcessController {

    @Autowired
    ConfigurationProcess configurationProcess;

    @PostMapping(value = "/update")
    @ApiOperation(value = "Set device configuration", notes = "Perform the configuration process")
    @Loggable
    @PreAuthorize("hasAnyRole('ROLE_MANAGER')")
    public ResponseEntity<Device> updateConfigurationDevice(@Valid @RequestBody ConfigurationDTO configurationDTO) {
        return new ResponseEntity<>(configurationProcess.configureDevice(configurationDTO.getDeviceId(), configurationDTO.getIcc()), HttpStatus.OK);
    }

    @PostMapping(value = "/remove/{deviceId}")
    @ApiOperation(value = "Remove device configuration", notes = "Remove the configuration from received device")
    @Loggable
    @PreAuthorize("hasAnyRole('ROLE_MANAGER')")
    public ResponseEntity<Device> getDevicesBySIMStatus(@PathVariable Long deviceId) {
        return new ResponseEntity<>(configurationProcess.removeConfiguration(deviceId), HttpStatus.OK);
    }

}
