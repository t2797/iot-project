package com.process.iotproject.api.controller;

import com.process.iotproject.api.model.JWTToken;
import com.process.iotproject.api.model.LoginRequest;
import com.process.iotproject.model.utils.Loggable;
import com.process.iotproject.services.security.SecurityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/login")
@Api(value = "Login controller", tags = "Login controller")
public class LoginController {

    @Autowired
    SecurityService securityService;

    @PostMapping()
    @Loggable
    @ApiOperation(value = "Login method", notes = "Perform login process. Return a JWT token.")
    public ResponseEntity<JWTToken> authenticate(@Valid @RequestBody LoginRequest loginRequest){
        String token = securityService.login(loginRequest.getUsername(), loginRequest.getPassword());
        return new ResponseEntity<>(new JWTToken(token), HttpStatus.OK);
    }

}
