package com.process.iotproject.api.controller;

import com.process.iotproject.model.entities.SIM;
import com.process.iotproject.model.utils.Loggable;
import com.process.iotproject.services.sim.SIMService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/sim", produces = "application/json")
@Api(value = "SIM controller", tags = "SIM controller")
public class SIMController {

    @Autowired
    SIMService simService;

    @GetMapping(value = "free")
    @ApiOperation(value = "Get SIM without devices", notes = "Return SIMs without associated devices")
    @Loggable
    public ResponseEntity<Page<SIM>> getSIMWithoutDevices(Pageable pageable) {
        return new ResponseEntity<>(simService.findSimNoRelatedDevice(pageable), HttpStatus.OK);
    }

}
