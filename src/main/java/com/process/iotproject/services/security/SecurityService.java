package com.process.iotproject.services.security;

import com.process.iotproject.configuration.properties.SecurityProperties;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.stream.Collectors;

@Service
public class SecurityService {

    static final String AUTHORITIES_KEY = "auth";
    static final String KEY_USERNAME = "USERNAME";

    private AuthenticationManager authenticationManager;

    private UserDetailsService userDetailsService;

    private SecurityProperties securityProperties;

    @Autowired
    public SecurityService(AuthenticationManager authenticationManager, UserDetailsService userDetailsService, SecurityProperties securityProperties) {
        this.authenticationManager = authenticationManager;
        this.userDetailsService = userDetailsService;
        this.securityProperties = securityProperties;
    }

    public String login(String username, String password) {


        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        return this.generateToken(userDetails);

    }

    /**
     * Token simple generation
     * @param userDetails user information
     * @return JWT token
     */
    public String generateToken(UserDetails userDetails) {

        String authorities = userDetails.getAuthorities().stream().map(GrantedAuthority::getAuthority)
                .collect(Collectors.joining(","));

        if(!StringUtils.hasText(authorities)) {
            authorities = "ROLE_ANONYMOUS";
        }

        HashMap<String, Object> userInfo = new HashMap<>();
        userInfo.put(KEY_USERNAME, userDetails.getUsername());
        userInfo.put(AUTHORITIES_KEY, authorities);

        Date validity = new Date(System.currentTimeMillis() + securityProperties.getValidity()*1000);

        return Jwts.builder()
                .setClaims(userInfo)
                .setSubject(userDetails.getUsername())
                .setExpiration(validity)
                .signWith(SignatureAlgorithm.HS512, securityProperties.getSecret())
                .compact();
    }

}
