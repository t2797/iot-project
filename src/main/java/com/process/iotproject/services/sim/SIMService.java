package com.process.iotproject.services.sim;

import com.process.iotproject.model.entities.SIM;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public interface SIMService {

    /**
     * Get a SIM by its unique identifier
     * @param icc unique identifier
     * @return selected sim
     */
    SIM findSimByIdentifier(@NotNull @NotBlank String icc);

    /**
     * Get a paginated list of SIM without associated device
     * @param pageable page information
     * @return page of SIMs
     */
    Page<SIM> findSimNoRelatedDevice(Pageable pageable);
}
