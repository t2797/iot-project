package com.process.iotproject.services.sim;

import com.process.iotproject.model.entities.SIM;
import com.process.iotproject.model.repositories.SIMRepository;
import com.process.iotproject.model.utils.Loggable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Service
public class SIMServiceImpl implements SIMService {

    @Autowired
    SIMRepository simRepository;

    @Override
    @Loggable
    public SIM findSimByIdentifier(@NotNull @NotBlank String icc) {
        return simRepository.findByIcc(icc).orElseThrow(() -> new EntityNotFoundException(String.format("SIM %s not found", icc)));
    }

    @Override
    @Loggable
    public Page<SIM> findSimNoRelatedDevice(Pageable pageable) {
        return simRepository.findByDeviceIsNull(pageable);
    }

}
