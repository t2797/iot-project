package com.process.iotproject.services.process;

import com.process.iotproject.configuration.properties.ApplicationProperties;
import com.process.iotproject.model.entities.Device;
import com.process.iotproject.model.entities.DeviceStatus;
import com.process.iotproject.model.entities.SIM;
import com.process.iotproject.model.repositories.DeviceStatusRepository;
import com.process.iotproject.model.utils.Loggable;
import com.process.iotproject.services.device.DeviceService;
import com.process.iotproject.services.sim.SIMService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import javax.validation.ValidationException;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Service
public class ConfigurationProcessImpl implements ConfigurationProcess{

    DeviceService deviceService;

    SIMService simService;

    ApplicationProperties applicationProperties;

    DeviceStatusRepository deviceStatusRepository;

    @Autowired
    public ConfigurationProcessImpl(DeviceService deviceService, SIMService simService, ApplicationProperties applicationProperties, DeviceStatusRepository deviceStatusRepository) {
        this.deviceService = deviceService;
        this.simService = simService;
        this.applicationProperties = applicationProperties;
        this.deviceStatusRepository = deviceStatusRepository;
    }

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    @Transactional
    @Loggable
    public Device configureDevice(@NotNull Long deviceId, @NotNull @NotBlank String simICC) {

        log.debug("Getting device, ID: {}", deviceId);
        Device device = deviceService.getDeviceById(deviceId);

        log.debug("Validating status ...");
        if(Long.compare(applicationProperties.getStatus().getReady(), device.getDeviceStatus().getId()) == 0 || device.getSim() != null) {
            throw new ValidationException(String.format("Device %d is already configured", device.getId()));
        }

        log.debug("Validating temperature ...");
        if (device.getTemperature() < applicationProperties.getTemperature().getMin() || device.getTemperature() > applicationProperties.getTemperature().getMax()) {
            throw new ValidationException(String.format("The temperature is not at the proper threshold. Current value %d", device.getTemperature()));
        }

        log.debug("Validating if SIM is free");
        SIM sim = simService.findSimByIdentifier(simICC);

        if(sim.getDevice() != null) {
            throw new ValidationException(String.format("SIM %s related to other device", simICC));
        }

        device.setSim(sim);
        DeviceStatus deviceStatus = deviceStatusRepository.findById(applicationProperties.getStatus().getReady()).orElseThrow(EntityNotFoundException::new);
        device.setDeviceStatus(deviceStatus);
        log.debug("Configuring device ...");
        return deviceService.update(device);
    }

    @Override
    @Transactional
    @Loggable
    public Device removeConfiguration(Long deviceId) {

        log.debug("Getting device, ID: {}", deviceId);
        Device device = deviceService.getDeviceById(deviceId);

        log.debug("Validating status ...");
        if(Long.compare(applicationProperties.getStatus().getNotReady(), device.getDeviceStatus().getId()) == 0 || device.getSim() == null) {
            throw new ValidationException(String.format("Device %d is not configured", device.getId()));
        }

        DeviceStatus deviceStatus = deviceStatusRepository.findById(applicationProperties.getStatus().getNotReady()).orElseThrow(EntityNotFoundException::new);
        device.setDeviceStatus(deviceStatus);
        device.setSim(null);
        log.debug("Removing configuration ...");
        return deviceService.update(device);
    }


}
