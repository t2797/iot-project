package com.process.iotproject.services.process;

import com.process.iotproject.model.entities.Device;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public interface ConfigurationProcess {

    /**
     * Perform the configuration process of a device. This process validate:
     *  - Temperature device is between -25 and 85 ºC
     *  - Device is not configured
     *  - Device has not a SIM related
     * @param deviceId device unique identifier
     * @param simICC sim unique identifier
     * @return device with SIM
     */
    Device configureDevice(@NotNull Long deviceId, @NotNull @NotBlank String simICC);

    /**
     * To remove a configured device is necessary remove the relationship with a SIM.
     * This process remove the relationship and set status not ready.
     * @param deviceId device unique identifier
     * @return device without SIM
     */
    Device removeConfiguration(@NotNull Long deviceId);

}
