package com.process.iotproject.services.device;

import com.process.iotproject.model.entities.Device;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;

@Validated
public interface DeviceService {

    /**
     * Return all devices with a related SIM by SIM status
     *
     * @param simStatus - SIM status
     * @param page      - information to perform to page the results
     * @return paginated list of devices that match with a related sim and the expected sim status
     */
    Page<Device> getDevicesBySIMStatus(@NotNull String simStatus, @NotNull Pageable page);

    /**
     * Return a page list of devices by status
     *
     * @param deviceStatus - device status unique identifier
     * @param page         - information to perform to page the results
     * @return paginated list of devices with the expected status
     */
    Page<Device> getDevicesByStatus(@NotNull String deviceStatus, @NotNull Pageable page);

    /**
     * Get a device by the unique identifier
     * @param id unique identifier of the device
     * @return complete information of the device
     */
    Device getDeviceById(@NotNull Long id);

    /**
     * Save a device into the database
     * @param device complete information of the device
     * @return new device
     */
    Device save(@NotNull Device device);

    /**
     * Update a device into the database
     * @param device complete information of the device
     * @return updated device
     */
    Device update(@NotNull Device device);
}
