package com.process.iotproject.services.device;

import com.process.iotproject.model.entities.Device;
import com.process.iotproject.model.repositories.DeviceRepository;
import com.process.iotproject.model.utils.Loggable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;

@Service
public class DeviceServiceImpl implements DeviceService {

    @Autowired
    DeviceRepository deviceRepository;

    /**
     * @see DeviceService#getDevicesBySIMStatus(String, Pageable)
     */
    @Override
    @Loggable
    public Page<Device> getDevicesBySIMStatus(@NotNull String simStatus, @NotNull Pageable page) {
        return deviceRepository.findAllBySim_SimStatus_Status(simStatus, page);
    }

    /**
     * @see DeviceService#getDevicesByStatus(String, Pageable)
     */
    @Override
    @Loggable
    public Page<Device> getDevicesByStatus(@NotNull String deviceStatus, @NotNull Pageable page) {
        return deviceRepository.findAllByDeviceStatus_Status(deviceStatus, page);
    }

    /**
     * @see DeviceService#getDeviceById(Long)
     */
    @Override
    @Loggable
    public Device getDeviceById(@NotNull Long id) {
        return deviceRepository.findById(id).orElseThrow(() -> new EntityNotFoundException(String.format("Device %d not found", id)));
    }

    /**
     * @see DeviceService#save(Device)
     */
    @Override
    @Transactional
    @Loggable
    public Device save(@NotNull Device device) {

        if(deviceRepository.findByName(device.getName()).isPresent()) {
            throw new EntityExistsException(String.format("Device %s exists in the database", device.getName()));
        }
        return deviceRepository.save(device);
    }

    @Override
    @Transactional
    @Loggable
    public Device update(@NotNull Device device) {

        Assert.notNull(device.getId(), "Device ID is mandatory");
        return deviceRepository.save(device);
    }

}
