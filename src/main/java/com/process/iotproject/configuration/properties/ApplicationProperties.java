package com.process.iotproject.configuration.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "process.device")
@Data
public class ApplicationProperties {

   private Temperature temperature;
   private Status status;

   @Data
   public static class Temperature {
       private int min;
       private int max;
   }

   @Data
   public static class Status {
       private Long ready;
       private Long notReady;
   }


}
