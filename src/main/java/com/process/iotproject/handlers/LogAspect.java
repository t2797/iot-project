package com.process.iotproject.handlers;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LogAspect {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Pointcut("within(@org.springframework.stereotype.Repository *)")
    public void repositories() {}

    @Pointcut("@annotation(com.process.iotproject.model.utils.Loggable)")
    public void methods() {}

    @Around("methods() || repositories()")
    public Object logMethods(ProceedingJoinPoint joinPoint) throws Throwable {

        String methodClass = joinPoint.getSignature().getDeclaringTypeName();
        String method = joinPoint.getSignature().getName();

        if(log.isDebugEnabled()) {
            log.debug("Init method: {}, from class: {}. Parameters: {}", method, methodClass, joinPoint.getArgs());
        }

        Object result = joinPoint.proceed();

        if(log.isDebugEnabled()) {
            log.debug("End method: {}, from class: {}.", method, methodClass);
        }

        return result;
    }

}
