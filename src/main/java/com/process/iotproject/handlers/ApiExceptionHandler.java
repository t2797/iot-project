package com.process.iotproject.handlers;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.validation.ValidationException;

@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {


    @ExceptionHandler(value = {EntityNotFoundException.class})
    public ResponseStatusException handleNotFound(EntityNotFoundException e) {
        return new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
    }

    @ExceptionHandler(value = {EntityExistsException.class})
    public ResponseStatusException handleEntityFound(EntityNotFoundException e) {
        return new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage(), e);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = {ValidationException.class})
    public ResponseStatusException handleValidationError(ValidationException e) {
        return new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
    }

}
