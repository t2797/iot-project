package com.process.iotproject.api.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.process.iotproject.model.entities.Device;
import com.process.iotproject.model.entities.SIM;
import com.process.iotproject.services.device.DeviceService;
import com.process.iotproject.services.sim.SIMService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class SIMControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SIMService simService;

    private ResourceLoader resourceLoader;

    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        objectMapper = new ObjectMapper();
        resourceLoader = new DefaultResourceLoader();
    }

    @Test
    void getSIMWithoutDevices() throws Exception {

        Resource jsonFile = resourceLoader.getResource("classpath:json/sim/sims.json");
        List<SIM> sims = objectMapper.readValue(jsonFile.getFile(), new TypeReference<List<SIM>>() { });

        given(simService.findSimNoRelatedDevice(PageRequest.of(0, 10, Sort.by("id").ascending()))).willReturn(new PageImpl<>(sims));

        mockMvc.perform(get("/sim/free")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("page", String.valueOf(0))
                        .param("size", String.valueOf(10))
                        .param("sort", "id,asc"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(2)))
                .andExpect(jsonPath("$.content[0].icc", is("8912343128761726354")));

        verify(simService, times(1)).findSimNoRelatedDevice(PageRequest.of(0, 10, Sort.by("id").ascending()));
        reset(simService);

    }
}