package com.process.iotproject.api;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.process.iotproject.model.entities.Device;
import com.process.iotproject.services.device.DeviceService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class DeviceControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DeviceService deviceService;

    private ResourceLoader resourceLoader;

    private ObjectMapper objectMapper;

    @BeforeEach
    private void init() {
        objectMapper = new ObjectMapper();
        resourceLoader = new DefaultResourceLoader();
    }


    @Test
    void getDevicesBySIMStatus_success() throws Exception {

        Resource jsonFile = resourceLoader.getResource("classpath:json/devices/devices_sim_status.json");
        List<Device> devices = objectMapper.readValue(jsonFile.getFile(), new TypeReference<List<Device>>() { });

        given(deviceService.getDevicesBySIMStatus("Blocked", PageRequest.of(0, 10, Sort.by("id").ascending()))).willReturn(new PageImpl<>(devices));

        mockMvc.perform(get("/devices/status/sim")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("simStatus", "Blocked")
                        .param("page", String.valueOf(0))
                        .param("size", String.valueOf(10))
                        .param("sort", "id,asc"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(2)))
                .andExpect(jsonPath("$.content[0].name", is("DEV1")));

        verify(deviceService, times(1)).getDevicesBySIMStatus("Blocked", PageRequest.of(0, 10, Sort.by("id").ascending()));
        reset(deviceService);
    }


    @Test
    void getDevicesBySIMStatus_fails_400() throws Exception {

        mockMvc.perform(get("/devices/status/sim")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        verify(deviceService, times(0)).getDevicesBySIMStatus("Blocked", PageRequest.of(0, 10));
        reset(deviceService);
    }

    @Test
    void getDevicesByStatus() throws Exception {

        Resource jsonFile = resourceLoader.getResource("classpath:json/devices/devices_status.json");
        List<Device> devices = objectMapper.readValue(jsonFile.getFile(), new TypeReference<List<Device>>() { });

        given(deviceService.getDevicesByStatus("READY", PageRequest.of(0, 10, Sort.by("id").ascending()))).willReturn(new PageImpl<>(devices));

        mockMvc.perform(get("/devices/status")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("status", "READY")
                        .param("page", String.valueOf(0))
                        .param("size", String.valueOf(10))
                        .param("sort", "id,asc"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(3)))
                .andExpect(jsonPath("$.content[0].name", is("DEV1")));

        verify(deviceService, times(1)).getDevicesByStatus("READY", PageRequest.of(0, 10, Sort.by("id").ascending()));
        reset(deviceService);
    }

    @Test
    void getDevicesByStatus_fails_400() throws Exception {

        mockMvc.perform(get("/devices/status/sim")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        verify(deviceService, times(0)).getDevicesByStatus("Blocked", PageRequest.of(0, 10));
        reset(deviceService);
    }
}