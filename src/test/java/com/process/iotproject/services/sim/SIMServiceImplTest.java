package com.process.iotproject.services.sim;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.process.iotproject.model.entities.Device;
import com.process.iotproject.model.entities.SIM;
import com.process.iotproject.model.repositories.SIMRepository;
import com.process.iotproject.services.device.DeviceService;
import com.process.iotproject.services.device.DeviceServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@SpringBootTest
class SIMServiceImplTest {


    @Mock
    SIMRepository simRepository;

    private ResourceLoader resourceLoader;

    @InjectMocks
    private SIMService simService = new SIMServiceImpl();

    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {

        resourceLoader = new DefaultResourceLoader();
        objectMapper = new ObjectMapper();
    }

    @Test
    void findSimByIdentifier_sucess() throws IOException {

        Resource jsonFile = resourceLoader.getResource("classpath:json/sim/sim.json");
        SIM sim = objectMapper.readValue(jsonFile.getFile(), SIM.class);

        when(simRepository.findByIcc(sim.getIcc())).thenReturn(Optional.of(sim));
        assertThat(simService.findSimByIdentifier(sim.getIcc())).isNotNull();
        verify(simRepository, times(1)).findByIcc(sim.getIcc());

    }

    @Test
    void findSimNoRelatedDevice_sucess() throws IOException {

        Resource jsonFile = resourceLoader.getResource("classpath:json/sim/sims.json");
        List<SIM> sim = objectMapper.readValue(jsonFile.getFile(),  new TypeReference<List<SIM>>() { });

        when(simRepository.findByDeviceIsNull(PageRequest.of(0, 10))).thenReturn(new PageImpl<>(sim));
        assertThat(simService.findSimNoRelatedDevice(PageRequest.of(0, 10)).getTotalElements()).isEqualTo(2L);
        verify(simRepository, times(1)).findByDeviceIsNull(any(PageRequest.class));

    }
}