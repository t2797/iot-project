package com.process.iotproject.services.process;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.process.iotproject.configuration.properties.ApplicationProperties;
import com.process.iotproject.model.entities.Device;
import com.process.iotproject.model.entities.DeviceStatus;
import com.process.iotproject.model.entities.SIM;
import com.process.iotproject.model.repositories.DeviceRepository;
import com.process.iotproject.model.repositories.DeviceStatusRepository;
import com.process.iotproject.model.repositories.SIMRepository;
import com.process.iotproject.services.device.DeviceService;
import com.process.iotproject.services.device.DeviceServiceImpl;
import com.process.iotproject.services.sim.SIMService;
import com.process.iotproject.services.sim.SIMServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.TestPropertySource;

import javax.persistence.EntityNotFoundException;
import javax.validation.ValidationException;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
@EnableConfigurationProperties(value = ApplicationProperties.class)
@TestPropertySource("classpath:application.yml")
class ConfigurationProcessImplTest {


    private ResourceLoader resourceLoader;

    @Mock
    private DeviceService deviceServiceImpl;

    @Mock
    private SIMService simService;

    @Mock
    private DeviceStatusRepository deviceStatusRepository;

    @Autowired
    ApplicationProperties applicationProperties;

    @InjectMocks
    private ConfigurationProcessImpl configurationProcess;

    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        this.resourceLoader = new DefaultResourceLoader();
        this.objectMapper = new ObjectMapper();
        this.configurationProcess = new ConfigurationProcessImpl(deviceServiceImpl, simService, applicationProperties, deviceStatusRepository);
    }

    @Test
    void configureDevice_success() throws IOException {

        Resource jsonDevice = resourceLoader.getResource("classpath:json/devices/device_not_ready.json");
        Device device = objectMapper.readValue(jsonDevice.getFile(), Device.class);

        Resource jsonSIM = resourceLoader.getResource("classpath:json/sim/sim.json");
        SIM sim = objectMapper.readValue(jsonSIM.getFile(), SIM.class);

        DeviceStatus deviceStatus = new DeviceStatus();
        deviceStatus.setId(1L);
        deviceStatus.setStatus("READY");

        when(deviceServiceImpl.getDeviceById(1L)).thenReturn(device);
        when(simService.findSimByIdentifier(sim.getIcc())).thenReturn(sim);
        when(deviceStatusRepository.findById(1L)).thenReturn(Optional.of(deviceStatus));
        when(deviceServiceImpl.update(device)).thenReturn(device);

        assertThat(configurationProcess.configureDevice(device.getId(), sim.getIcc())).isNotNull();
        verify(deviceServiceImpl, times(1)).getDeviceById(any(Long.class));
        verify(deviceServiceImpl, times(1)).update(any(Device.class));
        verify(simService, times(1)).findSimByIdentifier(any(String.class));
        verify(deviceStatusRepository, times(1)).findById(any(Long.class));


    }

    @Test
    void configureDevice_fail_deviceReady() throws IOException {

        Resource jsonDevice = resourceLoader.getResource("classpath:json/devices/device.json");
        Device device = objectMapper.readValue(jsonDevice.getFile(), Device.class);

        when(deviceServiceImpl.getDeviceById(1L)).thenReturn(device);

        assertThrows(ValidationException.class, () -> configurationProcess.configureDevice(1L, "8912343128761726354"));
        verify(deviceServiceImpl, times(1)).getDeviceById(any(Long.class));
        verify(deviceServiceImpl, times(0)).update(any(Device.class));
        verify(simService, times(0)).findSimByIdentifier(any(String.class));
        verify(deviceStatusRepository, times(0)).findById(any(Long.class));


    }

    @Test
    void configureDevice_fail_deviceWithSIM() throws IOException {

        Resource jsonDevice = resourceLoader.getResource("classpath:json/devices/device_not_ready.json");
        Device device = objectMapper.readValue(jsonDevice.getFile(), Device.class);
        device.setSim(new SIM());
        when(deviceServiceImpl.getDeviceById(1L)).thenReturn(device);

        assertThrows(ValidationException.class, () -> configurationProcess.configureDevice(1L, "8912343128761726354"));
        verify(deviceServiceImpl, times(1)).getDeviceById(any(Long.class));
        verify(deviceServiceImpl, times(0)).update(any(Device.class));
        verify(simService, times(0)).findSimByIdentifier(any(String.class));
        verify(deviceStatusRepository, times(0)).findById(any(Long.class));


    }

    @Test
    void configureDevice_fail_deviceLowTemperature() throws IOException {

        Resource jsonDevice = resourceLoader.getResource("classpath:json/devices/device_not_ready.json");
        Device device = objectMapper.readValue(jsonDevice.getFile(), Device.class);
        device.setTemperature(-40);
        when(deviceServiceImpl.getDeviceById(1L)).thenReturn(device);

        assertThrows(ValidationException.class, () -> configurationProcess.configureDevice(1L, "8912343128761726354"));
        verify(deviceServiceImpl, times(1)).getDeviceById(any(Long.class));
        verify(deviceServiceImpl, times(0)).update(any(Device.class));
        verify(simService, times(0)).findSimByIdentifier(any(String.class));
        verify(deviceStatusRepository, times(0)).findById(any(Long.class));


    }

    @Test
    void configureDevice_fail_deviceHighTemperature() throws IOException {

        Resource jsonDevice = resourceLoader.getResource("classpath:json/devices/device_not_ready.json");
        Device device = objectMapper.readValue(jsonDevice.getFile(), Device.class);
        device.setTemperature(90);
        when(deviceServiceImpl.getDeviceById(1L)).thenReturn(device);

        assertThrows(ValidationException.class, () -> configurationProcess.configureDevice(1L, "8912343128761726354"));
        verify(deviceServiceImpl, times(1)).getDeviceById(any(Long.class));
        verify(deviceServiceImpl, times(0)).update(any(Device.class));
        verify(simService, times(0)).findSimByIdentifier(any(String.class));
        verify(deviceStatusRepository, times(0)).findById(any(Long.class));


    }

    @Test
    void configureDevice_fail_simWithDevice() throws IOException {

        Resource jsonDevice = resourceLoader.getResource("classpath:json/devices/device_not_ready.json");
        Device device = objectMapper.readValue(jsonDevice.getFile(), Device.class);

        Resource jsonSIM = resourceLoader.getResource("classpath:json/sim/sim.json");
        SIM sim = objectMapper.readValue(jsonSIM.getFile(), SIM.class);
        sim.setDevice(new Device());
        when(deviceServiceImpl.getDeviceById(1L)).thenReturn(device);
        when(simService.findSimByIdentifier(sim.getIcc())).thenReturn(sim);
        assertThrows(ValidationException.class, () -> configurationProcess.configureDevice(1L, "8912343128761726354"));
        verify(deviceServiceImpl, times(1)).getDeviceById(any(Long.class));
        verify(deviceServiceImpl, times(0)).update(any(Device.class));
        verify(simService, times(1)).findSimByIdentifier(any(String.class));
        verify(deviceStatusRepository, times(0)).findById(any(Long.class));


    }
    @Test
    void configureDevice_fail_DeviceStatus() throws IOException {

        Resource jsonDevice = resourceLoader.getResource("classpath:json/devices/device_not_ready.json");
        Device device = objectMapper.readValue(jsonDevice.getFile(), Device.class);

        Resource jsonSIM = resourceLoader.getResource("classpath:json/sim/sim.json");
        SIM sim = objectMapper.readValue(jsonSIM.getFile(), SIM.class);

        when(deviceServiceImpl.getDeviceById(1L)).thenReturn(device);
        when(simService.findSimByIdentifier(sim.getIcc())).thenReturn(sim);
        when(deviceStatusRepository.findById(2L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> configurationProcess.configureDevice(1L, "8912343128761726354"));
        verify(deviceServiceImpl, times(1)).getDeviceById(any(Long.class));
        verify(deviceServiceImpl, times(0)).update(any(Device.class));
        verify(simService, times(1)).findSimByIdentifier(any(String.class));
        verify(deviceStatusRepository, times(1)).findById(any(Long.class));


    }

    @Test
    void removeConfiguration() throws IOException {

        Resource jsonDevice = resourceLoader.getResource("classpath:json/devices/device_sim.json");
        Device device = objectMapper.readValue(jsonDevice.getFile(), Device.class);

        DeviceStatus deviceStatus = new DeviceStatus();
        deviceStatus.setId(2L);
        deviceStatus.setStatus("NOT READY");

        when(deviceServiceImpl.getDeviceById(1L)).thenReturn(device);
        when(deviceStatusRepository.findById(2L)).thenReturn(Optional.of(deviceStatus));
        when(deviceServiceImpl.update(device)).thenReturn(device);

        assertThat(configurationProcess.removeConfiguration(device.getId())).isNotNull();
        verify(deviceServiceImpl, times(1)).getDeviceById(any(Long.class));
        verify(deviceServiceImpl, times(1)).update(any(Device.class));
        verify(deviceStatusRepository, times(1)).findById(any(Long.class));

    }

    @Test
    void removeConfiguration_fail_notReady() throws IOException {

        Resource jsonDevice = resourceLoader.getResource("classpath:json/devices/device_not_ready.json");
        Device device = objectMapper.readValue(jsonDevice.getFile(), Device.class);

        when(deviceServiceImpl.getDeviceById(1L)).thenReturn(device);

        assertThrows(ValidationException.class, () -> configurationProcess.removeConfiguration(1L));
        verify(deviceServiceImpl, times(1)).getDeviceById(any(Long.class));
        verify(deviceServiceImpl, times(0)).update(any(Device.class));
        verify(deviceStatusRepository, times(0)).findById(any(Long.class));

    }

    @Test
    void removeConfiguration_fail_notReady_notSim() throws IOException {

        Resource jsonDevice = resourceLoader.getResource("classpath:json/devices/device.json");
        Device device = objectMapper.readValue(jsonDevice.getFile(), Device.class);

        when(deviceServiceImpl.getDeviceById(1L)).thenReturn(device);

        assertThrows(ValidationException.class, () -> configurationProcess.removeConfiguration(1L));
        verify(deviceServiceImpl, times(1)).getDeviceById(any(Long.class));
        verify(deviceServiceImpl, times(0)).update(any(Device.class));
        verify(deviceStatusRepository, times(0)).findById(any(Long.class));

    }

    @Test
    void removeConfiguration_fail_DeviceStatus() throws IOException {

        Resource jsonDevice = resourceLoader.getResource("classpath:json/devices/device_sim.json");
        Device device = objectMapper.readValue(jsonDevice.getFile(), Device.class);

        when(deviceServiceImpl.getDeviceById(1L)).thenReturn(device);
        when(deviceStatusRepository.findById(2L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> configurationProcess.removeConfiguration(1L));
        verify(deviceServiceImpl, times(1)).getDeviceById(any(Long.class));
        verify(deviceServiceImpl, times(0)).update(any(Device.class));
        verify(deviceStatusRepository, times(1)).findById(any(Long.class));


    }
}