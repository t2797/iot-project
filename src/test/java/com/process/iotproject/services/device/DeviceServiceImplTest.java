package com.process.iotproject.services.device;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.process.iotproject.model.entities.Device;
import com.process.iotproject.model.repositories.DeviceRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@SpringBootTest
class DeviceServiceImplTest {

    @Mock
    DeviceRepository deviceRepository;

    private ResourceLoader resourceLoader;

    @InjectMocks
    private DeviceService deviceServiceImpl = new DeviceServiceImpl();

    private ObjectMapper objectMapper;

    @BeforeEach
    void init()  {
        resourceLoader = new DefaultResourceLoader();
        objectMapper = new ObjectMapper();
    }

    @Test
    void getDevicesBySIMStatus_success() throws IOException {

        Resource jsonFile = resourceLoader.getResource("classpath:json/devices/devices.json");
        List<Device> devices = objectMapper.readValue(jsonFile.getFile(), new TypeReference<List<Device>>() { });

        when(deviceRepository.findAllBySim_SimStatus_Status("Blocked", PageRequest.of(0, 10))).thenReturn(new PageImpl<>(devices));

        assertThat(deviceServiceImpl.getDevicesBySIMStatus("Blocked", PageRequest.of(0, 10)).getTotalElements()).isEqualTo(1L);
        assertThat(deviceServiceImpl.getDevicesBySIMStatus("Blocked", PageRequest.of(0, 10)).get().findFirst().get().getId()).isEqualTo(1L);
        verify(deviceRepository, times(2)).findAllBySim_SimStatus_Status("Blocked", PageRequest.of(0, 10));
    }


    @Test
    void getDevicesByStatus_success() throws IOException {

        Resource jsonFile = resourceLoader.getResource("classpath:json/devices/devices_status.json");
        List<Device> devices = objectMapper.readValue(jsonFile.getFile(), new TypeReference<List<Device>>() { });

        when(deviceRepository.findAllByDeviceStatus_Status("READY", PageRequest.of(0, 10))).thenReturn(new PageImpl<>(devices));
        assertThat(deviceServiceImpl.getDevicesByStatus("READY", PageRequest.of(0, 10)).getTotalElements()).isEqualTo(3L);
        verify(deviceRepository, times(1)).findAllByDeviceStatus_Status("READY", PageRequest.of(0, 10));

    }

    @Test
    void getDeviceById_notFound() {

        when(deviceRepository.findById(1L)).thenReturn(Optional.empty());
        assertThrows(EntityNotFoundException.class, () -> deviceServiceImpl.getDeviceById(1L));

    }

    @Test
    void getDeviceById_success() throws IOException {

        Resource jsonFile = resourceLoader.getResource("classpath:json/devices/device.json");
        Device device = objectMapper.readValue(jsonFile.getFile(), Device.class);

        when(deviceRepository.findById(1L)).thenReturn(Optional.of(device));
        assertThat(deviceServiceImpl.getDeviceById(1L)).isNotNull();
        verify(deviceRepository, times(1)).findById(1L);

    }

    @Test
    void save_fails() throws IOException {

        Resource jsonFile = resourceLoader.getResource("classpath:json/devices/device.json");
        Device device = objectMapper.readValue(jsonFile.getFile(), Device.class);

        when(deviceRepository.findByName(device.getName())).thenReturn(Optional.of(device));
        assertThrows(EntityExistsException.class, () -> deviceServiceImpl.save(device));
        verify(deviceRepository, times(0)).save(any(Device.class));
    }

    @Test
    void save_success() throws IOException {

        Resource jsonFile = resourceLoader.getResource("classpath:json/devices/device.json");
        Device device = objectMapper.readValue(jsonFile.getFile(), Device.class);
        when(deviceRepository.findByName(device.getName())).thenReturn(Optional.empty());
        when(deviceRepository.save(device)).thenReturn(device);
        assertThat(deviceServiceImpl.save(device)).isNotNull();
        verify(deviceRepository, times(1)).save(any(Device.class));
    }

   @Test
    void update_fails() throws IOException {
        Resource jsonFile = resourceLoader.getResource("classpath:json/devices/device.json");
        Device device = objectMapper.readValue(jsonFile.getFile(), Device.class);
        device.setId(null);
        assertThrows(IllegalArgumentException.class, () -> deviceServiceImpl.update(device));
        verify(deviceRepository, times(0)).save(any(Device.class));
    }

    @Test
    void update_success() throws IOException {
        Resource jsonFile = resourceLoader.getResource("classpath:json/devices/device.json");
        Device device = objectMapper.readValue(jsonFile.getFile(), Device.class);
        when(deviceRepository.save(device)).thenReturn(device);
        assertThat(deviceServiceImpl.update(device)).isNotNull();
        verify(deviceRepository, times(1)).save(any(Device.class));
    }

}