package com.process.iotproject.services.security;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class SecurityServiceTest {

    @Autowired
    SecurityService securityService;

    @Test
    void login() {
        assertThat(securityService.login("manager", "password")).isNotEmpty();
    }

    @Test
    void generateToken() {

        UserDetails userDetails = User.builder()
                .username("manager")
                .password("password")
                .roles("MANAGER")
                .build();

        assertThat(securityService.generateToken(userDetails)).isNotEmpty();

    }
}