package com.process.iotproject.model.repositories;

import com.process.iotproject.model.entities.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
class DeviceRepositoryTest {

    @Autowired
    DeviceRepository deviceRepository;

    private Device device;

    @BeforeEach
    void setUp() {
        device = new Device();
        device.setName("dev2");
        device.setTemperature(21);
        device.setDeviceStatus(new DeviceStatus(1L, "READY"));
    }

    @Test
    void findAll(){
        assertThat(deviceRepository.findAll().size()).isEqualTo(4L);
    }

    @Test
    void findAll_pageAndOrder(){

        Pageable pageable = PageRequest.of(0, 5, Sort.by("id").descending());
        Page<Device> page = deviceRepository.findAll(pageable);
        assertThat(page.getTotalPages()).isEqualTo(1L);
        assertThat(page.get().count()).isEqualTo(4L);
        assertThat(page.getTotalElements()).isEqualTo(4L);
        assertThat(page.get().findFirst().get().getId()).isEqualTo(4L);
    }

    @Test
    void findById() {
        assertThat(deviceRepository.findById(1L).get().getId()).isEqualTo(1L);
    }

    @Test
    void findById_notFound() {
        assertThat(deviceRepository.findById(10L)).isEmpty();
    }

    @Test
    void findByStatus_id() {
        assertThat(deviceRepository.findAllByDeviceStatus_Id(1L, PageRequest.of(0, 10))).isNotEmpty();
    }

    @Test
    void findByStatus() {
        assertThat(deviceRepository.findAllByDeviceStatus_Status("READY", PageRequest.of(0, 10))).isNotEmpty();
    }

    @Test
    void findByStatus_andRelatedSIM() {
        assertThat(deviceRepository.findAllByDeviceStatus_StatusAndSimIsNotNull("READY", PageRequest.of(0, 10))).isNotEmpty();
    }

    @Test
    void findByTemperature() {
        assertThat(deviceRepository.findAllByTemperature(-4, PageRequest.of(0, 10))).isNotEmpty();
    }

    @Test
    void findByTemperatureAndStatus() {
        assertThat(deviceRepository.findAllByTemperatureAndDeviceStatus_Id(-4, 1L, PageRequest.of(0, 10))).isNotEmpty();
    }

    @Test
    void findDevice_unrelated() {
        assertThat(deviceRepository.findAllBySimIsNull(PageRequest.of(0, 10)).getTotalElements()).isEqualTo(2L);
    }

    @Test
    void findDevice_related() {
        assertThat(deviceRepository.findAllBySimIsNotNull(PageRequest.of(0, 10)).getTotalElements()).isEqualTo(2L);
    }

    @Test
    void saveDevice() {
        assertNotNull(deviceRepository.save(device));
    }

    @Test
    @Transactional
    void saveDevice_withRelatedSIM() {
        SIM sim = new SIM();
        sim.setIcc("8977885533886677887");
        sim.setCountry(new Country(1L, "Spain"));
        sim.setOperator(new Operator(1L, "Vodafone"));
        sim.setSimStatus(new SIMStatus(1L, "Active"));
        device.setSim(sim);
        Device sDevice = deviceRepository.save(device);
        assertNotNull(sDevice);
        assertNotNull(sDevice.getSim());
    }

    @Test
    void findDeviceBySimStatus_Id() {
        assertThat(deviceRepository.findAllBySim_SimStatus_Id(2L, PageRequest.of(0, 10)).getTotalElements()).isEqualTo(2L);
    }

    @Test
    void findDeviceBySimStatus() {
        assertThat(deviceRepository.findAllBySim_SimStatus_Status("Waiting for activation", PageRequest.of(0, 10)).getTotalElements()).isEqualTo(2L);
    }

    @Test
    void saveDevice_validationName() {
        device.setName(null);
        assertThrows(ConstraintViolationException.class, () -> {
            deviceRepository.save(device);
        });
    }

    @Test
    void saveDevice_validationStatus() {
        device.setDeviceStatus(null);
        assertThrows(ConstraintViolationException.class, () -> {
            deviceRepository.save(device);
        });
    }

    @Test
    void deleteDevice() {
        Device testDevice = deviceRepository.save(device);
        Long id = testDevice.getId();
        deviceRepository.delete(testDevice);
        assertThat(deviceRepository.findById(id)).isEmpty();
    }

    @Test
    @Transactional
    void findDeviceByName_success() {
        assertThat(deviceRepository.findByName("DEV1")).isNotEmpty();
    }
}