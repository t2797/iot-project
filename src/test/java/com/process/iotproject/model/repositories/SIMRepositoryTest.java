package com.process.iotproject.model.repositories;

import com.process.iotproject.model.entities.Country;
import com.process.iotproject.model.entities.Operator;
import com.process.iotproject.model.entities.SIM;
import com.process.iotproject.model.entities.SIMStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import javax.validation.ConstraintViolationException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
class SIMRepositoryTest {

    @Autowired
    SIMRepository simRepository;

    private SIM sim;

    private Country country;

    @BeforeEach
    public void createEntities() {

        // SIM
        sim = new SIM();
        sim.setId(8L);
        sim.setIcc("8977885533886677887");
        sim.setCountry(new Country(1L, "Spain"));
        sim.setOperator(new Operator(1L, "Vodafone"));
        sim.setSimStatus(new SIMStatus(1L, "Active"));
    }

    @Test
    void findAll(){
        assertThat(simRepository.findAll().size()).isEqualTo(7L);
    }

    @Test
    void findAll_pageAndOrder(){

        Pageable pageable = PageRequest.of(0, 5, Sort.by("id").descending());
        Page<SIM> page = simRepository.findAll(pageable);
        assertThat(page.getTotalPages()).isEqualTo(2L);
        assertThat(page.get().count()).isEqualTo(5L);
        assertThat(page.getTotalElements()).isEqualTo(7L);
        assertThat(page.get().findFirst().get().getId()).isEqualTo(7L);
    }

    @Test
    void findById() {
        assertThat(simRepository.findById(2L).get().getId()).isEqualTo(2L);
    }

    @Test
    void findById_notFound() {
        assertThat(simRepository.findById(22L)).isEmpty();
    }

    @Test
    void findByICC() {
        assertThat(simRepository.findByIcc("8992341668761726354").get().getId()).isEqualTo(4L);
    }

    @Test
    void findByStatus() {
        assertThat(simRepository.findAllBySimStatus_Id(2L, PageRequest.of(0, 10)).getTotalElements()).isEqualTo(3L);
    }

    @Test
    void findSIM_unrelated() {
        assertThat(simRepository.findByDeviceIsNull(PageRequest.of(0, 10)).getTotalElements()).isEqualTo(5L);
    }

    @Test
    void findSIM_related() {
        assertThat(simRepository.findByDeviceIsNotNull().size()).isEqualTo(2L);
    }

    @Test
    void findSIM_relatedDeviceIsNotNull() {
        List<SIM> sims = simRepository.findByDeviceIsNotNull();
        assertThat(sims.size()).isEqualTo(2L);
        assertNotNull(sims.get(0).getDevice());
    }

    @Test
    void saveSIM() {
        Long id = simRepository.save(sim).getId();
        assertNotNull(id);
        assertNotNull(simRepository.findById(id));
    }

    @Test
    void saveSIM_validationIcc() {
        sim.setIcc("89121212121212121212");
        assertThrows(DataIntegrityViolationException.class, () -> {
           simRepository.save(sim);
        });
    }

    @Test
    void saveSIM_validationCountry() {
        sim.setCountry(null);
        assertThrows(ConstraintViolationException.class, () -> {
           simRepository.save(sim).getId();
        });
    }

    @Test
    void saveSIM_validationOperator() {
        sim.setOperator(null);
        assertThrows(ConstraintViolationException.class, () -> {
           simRepository.save(sim).getId();
        });
    }

    @Test
    void saveSIM_validationStatus() {
        sim.setSimStatus(null);
        assertThrows(ConstraintViolationException.class, () -> {
           simRepository.save(sim).getId();
        });
    }
    @Test
    void deleteSIM() {
        SIM testSIM = simRepository.save(sim);
        Long id = testSIM.getId();
        simRepository.delete(testSIM);
        assertThat(simRepository.findById(id)).isEmpty();
    }

}